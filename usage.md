# Using CHUGY effectively

CHUGY is a program to change uid/gid en-masse, on a given set of directories. For detailed help on how to run chugy, please run:

```$path/chugy -h```

This document explains how to run chugy effectively in production.

#Background

## What is change-uid/gid?

When you merge/integrate multiple sites with different username-userid-primary_groupid mapping, programs and access permissions don't work as expected. The best way to remedy this is to *merge the uid/gid* spaces. Typically, one namespace is considered the root (dominant) and the other namespace is converted/changed over to the dominant namespace.

One of the most important tasks after merging the uid/gid space is to move file ownerships from old uid/gid to the newly created uid/gid for the accounts that have conflicts in the dominant namespace.

This requires extensive inspection of each file's permissions in the converted namespace, and moving the uid/gid over.

# Usage of Chugy in 3 modes

## Mode1: Change recursively on a set of directories

```
	$path/chugy \
		--conf /change/map/file.ext  --summary --force \
		--logfile /tmp/mylog.log \
		--verbose \
		--dir /directory/to-be-changed/
```

In this mode, the map for "old" uid/gid to "new" uid/gid should be specified in 
the conf file (--conf argument). chugy will use that to inspect the directories
for any files with uid/gid matching the mapped uid/gid. Once it finds that a file's
ownership has a conversion map, it applies it to that file. This can take a long time.
To aid in keeping track, chugy offers the following ways to inspect the progress:

	1. Tail (-f) the logfile (--logfile argument). This is a fast changing file.
	2. watch your syslog (wherever user.info is logged)

If you specific the '--grain <N>' option, chugy *will log a line for every N files
processed*.

##Mode2: Fix what was missing during the first pass


```
	$path/chugy \
		--conf /NEW/map/file.ext  --summary --force \
		--logfile /tmp/mylog.log \
		--verbose \
		--infile /log/from/previous/run/showing_files_with_NOCHG
```

When you first run in Mode1, chugy will report any missing uid/gid mappings. These are
the uid/gid that were found in the file system, but *had no corresponding mapping in 
the uid/gid map file*. 

You should inspect these uid/gid reports in the logfile from the Mode1 run. Should you
find that some of those files do need a new owner/group, you should create a new map file
for applying changes to just those files (instead of re-running in the whole filesystem).

To make this possible, chugy writes logs in an easy to parse format. All logs entries are
<TAB> delimited, and have a regular format as follows:

```
LOG:

	timestamp <tab> facility <tab> message

For example:
	timestamp <tab>CHANGED <tab> file <tab> olduid <tab> oldgid <tab> newuid <tab> newgid
	timestamp <tab>NOCHG <tab> file <tab> olduid <tab> oldgid

When running in Mode2, chugy will simply parse this output, and re-apply the new map for
any file that had the "NOCHG" status in the log.

##Mode2: Undo the previous run

At times, you may want to undo the previous run completely. This may happen if you ran 
chugy before the scheduled time, or if the file system was not part of the mapped
namespace, or for any other reason whatsoever.

To undo, use the previous run's log file and do the following:

```
	$path/chugy \
		--conf /same/old/map/file.txt --summary --force \
		--logfile /new/log/file --verbose \
		--undo --infile /old/run/log/file.log
```


## Short form of options.

The following short form is allowed for each option:

```
	--conf:      -c
	--logfile:   -l
	--force:     -f
	--undo:      -u
	--infile:    -i
	--summary:   -s
	--verbose:   -v


## Exclude directories/files

To exclude some files/directories from being subject to uid/gid conversion, you can
add "--exclude <name>" to the chugy invocation. 

By default ".snapshot" and ".fastforward" directories are ignored. *As of this version,
these will be ignored from being processed, but every file under these directories
will be scanned*. If you don't want this behavior, submit feedback. We can change it.


