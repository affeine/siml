#!/bin/bash
TESTDIR=/tmp/trantor
rm -rf ${TESTDIR};
mkdir -p ${TESTDIR};

ME=$0
[ "x$SIML_ROOTDIR" = "x" ] && echo "set SIML_ROOTDIR to top directory and re-run" && exit;
[ ! -d $SIML_ROOTDIR ] && echo "Directory ${SIML_ROOTDIR} not found" && exit;

RELDIR="${SIML_ROOTDIR}/release"
BINDIR="${SIML_ROOTDIR}/bin"

CWD=`pwd`
FILES="chugy ../scripts/gen_test_files.py ../scripts/t/Test.map.txt ../scripts/runit"

cd ${RELDIR};
cp ${FILES} ${TESTDIR}
echo "All files under ${TESTDIR}"
ls -l $TESTDIR
echo;
cat <<EOF 
#
#run this command to perform a sample test";
#
cd ${TESTDIR}; sh runit
#
#to keep the trace of the run, do:
#
export SIML_KEEP_TRACE=1
EOF
