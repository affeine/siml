#!/bin/sh
#
#build the binaries
#
ME=$0
[ "x$SIML_ROOTDIR" = "x" ] && echo "set SIML_ROOTDIR to top directory and re-run" && exit;
[ ! -d $SIML_ROOTDIR ] && echo "Directory ${SIML_ROOTDIR} not found" && exit;

RELDIR="${SIML_ROOTDIR}/release"
BINDIR="${SIML_ROOTDIR}/bin"

CWD=`pwd`
FILES="chugy.pl morphy.pl pathtester.pl"

cd ${BINDIR};
for file in ${FILES}
do
  binfile=`echo $file|sed -e 's,\..*$,,'`
  echo "compiling binary ${binfile} from ${file}"
  pp -o ${RELDIR}/${binfile} ${BINDIR}/${file}
done
