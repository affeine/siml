#!/bin/bash
DIR=`dirname $0`
SIML_ROOTDIR=`dirname $DIR`
TEST_DATA=$HOME/xfr/linux-5.10.3.tar.xz
TEST_DIR=/tmp/linux-5.10.3
if [ ! -f $TEST_DATA ]; then
	echo "Data file ${TEST_DATA} not found. exit"
	exit;
fi

echo "Extracting linux sources to /tmp/";
#rm -rf /tmp/linux-5.10.3/; 
#(cd /tmp; tar xf $TEST_DATA); 

echo "Testing default flow: ";
${SIML_ROOTDIR}/release/pathtester -m ../test/full_example_paths.txt  -i  ../test/linux_src_files_gt_256kb.txt -c -v  2>/tmp/default_testgen.log
/usr/bin/time $SIML_ROOTDIR/release/morphy -c ../test/full_example_paths.txt -d /tmp/linux-5.10.3/ -l /tmp/morphy.log -v -f
echo "Done";
find /tmp/linux-5.10.3 -name '*.morphy' | xargs rm -f
tail /tmp/morphy.log | sed -e 's/^/DEFAULT> /'

echo "Testing auto flow: ";
$SIML_ROOTDIR/release/pathtester -m ../test/full_example_paths.txt  -i  ../test/linux_src_files_gt_256kb.txt -c -v  2>/tmp/auto_testgen.log
/usr/bin/time $SIML_ROOTDIR/release/morphy -c ../test/full_example_paths.txt -d /tmp/linux-5.10.3/ -l /tmp/morphy.log -v -f -a auto
tail /tmp/morphy.log | sed -e 's/^/AUTO> /'
echo;
echo "Cleaning up newly created files";
find /tmp/linux-5.10.3 -name '*.morphy' | xargs rm -f
echo "Check /tmp/morphy.log for detailed log info";
