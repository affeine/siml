
import tempfile as t
import sys
import csv
import os
import re

files = []
uidgid = []
uid = []

def create_file(filename, owner, group, newo, newg):
    global files
    global d
    files.append([filename, owner, group])
    with open(filename, 'w') as f:
        f.write(":".join([filename, 'uid='+owner, 'gid=' + group, 'nuid=' + newo, 'ngid=' + newg] )+ "\n")
    os.chown(filename, int(owner), int(group))
    print("Created file: ", filename)

#
#painful to cleanup. reject
#d = "{0}.{1}".format('/tmp/simltest',os.getpid())
#
d = '/tmp/simltest'
map = '/tmp/map_simltest.in'

if not os.path.exists(d):
    os.mkdir(d)

#STDIN needs to be a mapfile of format:
#  old_username:new_username:old_uid:old_gid:new_uid:new_gid
#
# the other formats are described in main directory
#
mapreader = csv.reader(sys.stdin, delimiter=":")

patt = re.compile('^[a-z]');

for line in mapreader:
  if patt.match(line[0]):
		if len(line) != 6:
			print(sys.stderr, "WARN>\tNot enough data, line ignored: {0}".format(":".join(line)))
			continue
		print("CREAT>\tCreating files for line: ", ":".join(['uidgid'] + line[2:]))
		create_file(d + '/' + line[0], line[2], line[4], line[3], line[5])
		uidgid.append(":".join(["uidgid", line[2], line[4], line[3], line[5]]))
		uid.append(":".join(["uid", line[2], line[3]]))
#
#create two bogus files that SHOULD NOT be changed
#
create_file( d + '/' + 'nouser1', "65534", "65534", "65534", "65534")
create_file( d + '/' + 'nouser2', "65533", "65533", "65533", "65533")

with open(map, 'w') as fh:
	for entry in uidgid:
		fh.write(entry + "\n")
	for entry in uid:
		fh.write(entry + "\n")
print("CREAT> Wrote map file '{0}'".format(map))
