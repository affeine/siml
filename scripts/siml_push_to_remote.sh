#!/bin/bash

SIML_BINS_DIR=/tmp/trantor
[ ! -d ${SIML_BINS_DIR} ] && echo "Cannot find ${SIML_BINS_DIR}; exiting" && exit;

CMD="(cd ${SIML_BINS_DIR}; tar xvzf -; pwd)"
echo "CMD = $CMD";
cd ${SIML_BINS_DIR};
tar czf - . | (ssh ramkib@xnd-spark.xilinx.com "$CMD");
