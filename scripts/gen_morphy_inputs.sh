#!/bin/bash
LINUX_VER="5.10.3"
LINUX_SRC="$HOME/xfr/linux-${LINUX_VER}.tar.xz"

[ ! -f ${LINUX_SRC} ] && echo "Source ${LINUX_SRC} not found" && exit;
DEST_DIR=/tmp
DEST="${DEST_DIR}/linux-${LINUX_VER}";

echo "Cleaning up old ${DEST}";

if [ -d ${DEST} ]; then
	echo "#Please cleanup ${DEST} by piping this output to /bin/bash -x: ";
	echo "rm -rf ${DEST}";
else 
	cd ${DEST_DIR};
	tar xvf ${LINUX_SRC};
fi
for i in `seq 1 8`
do
	let kb=2**i bytes=kb*1024
	echo "Generating file list for $bytes ($kb KB)";
	outfile="/tmp/linux_src_files_gt_${kb}kb.txt";
	find $DEST -type f -size +${bytes}c >${outfile}
done
wc -l /tmp/*kb.txt
