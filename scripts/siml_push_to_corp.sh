#!/bin/bash

export SIML_ROOTDIR="$HOME/code/siml";
export SIML_PUSHDIR="code/siml";

[ ! -d ${SIML_ROOTDIR} ] && echo "Cannot find ${SIML_ROOTDIR}; exiting" && exit;

cd ${SIML_ROOTDIR};
CMD="(cd ${SIML_PUSHDIR}; tar xvzf -; pwd)"
echo "CMD = $CMD";
tar czf - bin scripts | (ssh ramkib@xsjramkib40x.xilinx.com "$CMD");
