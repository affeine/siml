# SIML: scalable infrastructure management library

A set of libraries and utilities to perform scalable infrastructure
operations. These utilities do not require anything more than what
is available on a basic install of a modern unix system.

These utilities can be run on "head-end" nodes to accomplish
common operations needed for many 'transformation' type tasks.


## Utilities supported

* xprint: slice and dice text (field delimited) data on the cli
* fields: prints the header of a field-delimited file (line #1)
* whichp: find the version of a perl module (if available)
* chugy.pl: change user/group ids recursively

## TODO utilities

* linky.pl: change symlinks to point to new namespaces
* morphy.py: change namespaces within individual files
* chpath.pl: a perl/recursive implementation of morphy.py
* sysnap.compute: utility to gather metadata about a system
* sysnap.storage: utility to gather metadata about storage



## Examples:

* whichp Net::DNS
	Finds the version of Net::DNS and the location in @INC

* fields -F':' /your/file
	Finds the fieldnames (from first line) of /your/file

* xprint -F ","  -c 1 2 -v /your/file

	treats /your/file as csv delimited, prints out 2nd and 3rd columns
	(select column2, column3 from /your/file)

* xprint -c 3 /your/file

	prints the histogram of column 4 values (increasing %)

* chugy.pl -c /tmp/map1.txt  -d /cloud/XYZ -s -f -v -l /tmp/your_pid.log

  using /tmp/map1.txt as the uid/gid map, recure down /cloud/XYZ
  directory, force (-f) the change ownership, log (-l) to
  /tmp/your_pid.log in verbose (-v) mode, and print summary (-s)
  statistics at the end of the run



