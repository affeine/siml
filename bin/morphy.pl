#!/usr/bin/perl -w
		eval 'exec perl -S $0 "$@"'
				if 0;
#
# morphy.pl: change file path strings with a file by looking for all
# {oldpath} references and converting the to {newpath}. This allows for
# directory/file paths to be correct in the 'new' namespace for every
# 'old' pathname referencefor directory/file paths to be correct in the
# 'new' namespace for every 'old' pathname reference. For example, if you
# move filesystems that mount to /prem/this/ to /cloud/that, a file that
# references '/prem/this/x/y.c' will break in the new environment. To
# correctly function, the string has to change to '/cloud/that/x/y.c'.
# 
#	$Id$
#

use Pod::Usage qw(pod2usage);
use Getopt::Long;
use Sys::Syslog qw(:standard :macros);
use Sys::Hostname;
use File::Find;
use Time::HiRes qw(time);
use POSIX;

use strict;


#
#fn declarations here
#
sub morphy_log;
sub validate;
sub morphy_arg_parse;
sub morphy_log_init;
sub morphy_read_config;
sub Usage;

sub find_safe;
sub find_fast;	#to be implemented 
sub morph_namespace;
#
#

#
#vars here
#
my(%path_map, %stats, %g_nonword_chars, %g_first_char, $default_pattern);
my($conf, $log, $log_h, @dirs, $help, $program, $infile);
my($auto, $auto_maxlen, $auto_minlen) = (0, 0, 1_000_000);
my($auto_pattern, $auto_end_pattern, @name_W_chars);

my($total_changes, $total_files_with_changes) = (0,0);
my($force, $verbose, $follow_symlinks) = (0,0,0);
my($print_summary, $n_processed, $n_links) = (0,0,0);
my($fname, $dirname) = ('', '');
my $GRAIN = 100;

#
# sub definitions here
#
sub Usage {
	if ($_[0] and $_[0] =~ /^\d+/) {
		pod2usage('-verbose' => shift);
	}
	else {
		pod2usage('-verbose' => 1, '-message' => $_[0]);
	}
}

sub morphy_arg_parse {
	local(@ARGV) = @_;
	Usage() if (!GetOptions(
		'a|auto' => \$auto,
		'c|conf=s' => \$conf,
		'd|dir=s' => \@dirs,
		'f|force' => \$force,
		'g|grain=i' => \$GRAIN,
		'h|help' => \$help,
		'i|infile=s' => \$infile,
		'l|log=s' => \$log,
		's|summary' => \$print_summary,
		'v|verbose' => \$verbose,
		));
	Usage(2) if defined $help;
	Usage("Config File error: $!") unless (defined $conf and -r $conf);
	unless (defined $infile) {
		Usage("No directories to operate on!") unless (@dirs and grep -d, @dirs);
	}
	if (defined $infile and scalar(@dirs)) {
		Usage("Can only work on file ($infile) OR directories  (@dirs), not both!");
	}

	morphy_log_info("Dry run mode, no changes will be made!") unless defined $force;
}

#
#config file format is a simple map of from_path <tab> to_path
#For examples, run this program with '--help' option
#
sub morphy_read_config {
	my $f = shift;
	my $conf_h;
	my($from, $to, @rest, @chars, $found_paths);

	open($conf_h, '<', $conf) or die("Cannot read $conf: $!\n");
	morphy_log_info(">> Begin ReadConfig : ${conf}\n");
	while ( <$conf_h> ) {
		chomp;
		next unless m{^/\S+\t\S+(\s+#)?};	

#		print STDERR "$.\t$_\n";

		($from, $to) = (split /\t/)[0,1];

		$path_map{$from} = $to;

		$found_paths++;
		
		@chars = split //, $from;

		$g_first_char{$chars[0]}++;

		#map all find non-word chars in 'from' paths
		for my $i (1..$#chars) {
			$g_nonword_chars{ $chars[$i] }++ if $chars[$i] =~ /[^\w\/\s]/;
		}
	}
	close($conf_h);
	print(STDERR "Did not find any matching path name, exit.\n"), exit
		unless $found_paths;

	set_auto_pattern();
	set_max_min_length();

	morphy_log_info("<< End ReadConfig: ${conf}\n");
}

sub morphy_log_init {
	($program = $0 ) =~ s,^.+/,,;
	if (defined $log) {
		open($log_h, '>', $log) or die("Cannot write to log {$log}: $!\n");
	}
	else {
		$log_h = *STDOUT;
		print STDERR ">>> Opening STDOUT for writing\n";
	}

	#syslog
	openlog($program, 'ndelay,pid', 'user');

}

sub morphy_log {
	#simple logger
	return unless $verbose;

	my $t = sprintf("%.6f", time());
	defined $log_h ?
		print $log_h join("\t", $t, @_, "\n") :
		print STDERR join("\t", $t, @_, "\n");
}

#AUTOLOAD some day
sub morphy_log_info { morphy_log "INFO", @_; }
sub morphy_log_changed { morphy_log "CHANGED", @_; }
sub morphy_log_nochange { morphy_log "NOCHG", @_; }
sub morphy_log_debug { morphy_log "DEBUG", @_; }
sub morphy_log_status { morphy_log "STATUS", @_; }
sub morphy_log_summary { morphy_log "SUMMARY", @_; }
sub morphy_log_scan { morphy_log "SCANNED", @_; }

sub validate {
	if (($< == 0 or $> == 0) and defined $force) {
		morphy_log_info("Running as root. No undo possible!");
	}
	unless (defined $infile) {
		die("Cannot modify any directories given!\n") unless grep -w, @dirs;
	};
}

sub set_auto_pattern {

	#default pattern for file name characters in unix
	#
	#IMPORTANT: if you find any characters other than the below within
	#any 'from' path names in your map file, add them to the array.
	#
	my(@name_W_chars) = qw(- . _);
	$auto_pattern = '(/[\w' . join('', map { '\\' . $_ } @name_W_chars) . ']+)';
	$auto_end_pattern = '\b';
	#print STDERR "(${auto_pattern})$auto_end_pattern\n";
}

sub set_max_min_length {
	#find minimum and maximum directory levels in 'from' paths	
	my($max, $min, $len) = (0, 1_000_000, 0);
	for my $k (keys %path_map) {
		$len = scalar split m{/}, $k;
		$max = $len if $len > $max;
		$min = $len if ($len > 0 and $len < $min);
	}
	$auto_minlen = $min - 1; 
	$auto_maxlen = $max - 1;

	#create a bruteforce pattern that matchs exactly those strings in
	#'from' path in map file. This is the last resort, and it may not
	#work in all cases.
	#
	#However, in some cases, it can work surprisingly faster than auto generated
	#pattern. 
	#
	$default_pattern = join '|', sort { length $b <=> length $a } keys %path_map;
	$default_pattern = '(' . $default_pattern . ')';

	#morphy_log_info("::DEFAULT pattern = $default_pattern\n");
	morphy_log_info("::Auto Pattern    = $auto_pattern ". " from {$auto_minlen, $auto_maxlen}\n");
}

sub morph_file {
	my($f, $auto) = @_;
	my($fh, $content);
	my $n_changes = 0;
	my($atime, $mtime);

	unless (open($fh, $f)) {
		warn("File ${f} open failure: $!\n");
		return;
	};

	#morphy_log_info("reporting from $f");
	#get file's last touch timestamp
	#
	($atime, $mtime) = (stat $f)[8,9];

	local $/ = undef;
	$content = <$fh>;
	$fh->close();
	if (defined $auto and $auto eq 'auto') {

		for (my $i = $auto_maxlen; $i >= $auto_minlen; $i--) {
			my $var_patt = "($auto_pattern" . "{$i})";

			#change when forced
			if ($force) {
				$content =~ s,${var_patt}${auto_end_pattern},exists $path_map{$1} ? $path_map{$1} : $1,egsmx;
			} else {
				while ($content =~ m{${var_patt}${auto_end_pattern}}gsmx) {
					$n_changes++ if exists $path_map{$1};
				}
				morphy_log_info("Total expected changes in file $f : $n_changes");
			}
		}
	}
	else {
		#brute force enumeration of all paths
		if ($force) {
			$content =~ s,${default_pattern}${auto_end_pattern},$path_map{$1},eogsmx;
		}
		else {
				while ($content =~ m{${default_pattern}${auto_end_pattern}}gsmx) {
					#morphy_log_info(" ---> inside pattern matching section");
					$n_changes++ if exists $path_map{$1};
				}
				morphy_log_info("Total expected changes in file $f : $n_changes");
		}
	}
	if ($force) {
		#if anything changed, we have to update the file
		my $orig_size = -s $f;
		my $d = $f;
		$d =~ s,/[^/]+$,,;

		if (length($content) != $orig_size) {
			$total_files_with_changes++;
			unless (open($fh, "> $f")) {
				morphy_log_info("FAILED> ${f} write error: $!");
				return;
			}
			$fh->print($content);
			$fh->close();
			utime($atime, $mtime, $f) or morphy_log_info("WARNING: can't reset atime/mtime for $f\n");
			utime($atime, $mtime, $d) and morphy_log_info("Reset atime/mtime for $d\n");
			morphy_log_changed("${f}");
		}
		else {
			morphy_log_nochange("${f}");
			$stats{'nochange'}++;
		}
	}
	elsif ($n_changes) {
		$total_files_with_changes++;
		$total_changes += $n_changes;
	}
}


#
#wrapper to the main worker function above
#
sub morph_namespace {
	if (@_) {
		$fname = shift;
	} else {
		$fname = $File::Find::name;
	}
	$_ = $fname;
	morphy_log_info("WARNING\tundefined file!"), return unless defined $_;

	$stats{'total'}++;
	if (-l) {
		#do nothing for symlinks, yet
		$stats{'nochange'}++;
		return;
	}

	$stats{'total'}--, return if ( m{^\.\.?$} );
	syslog(LOG_INFO, "Processed $stats{'total'} inodes") 
		if ($stats{'total'} % $GRAIN == 0);

  $stats{'nochange'}++, morphy_log_info("'$fname' not a text file.\n"),
		return unless -T $fname;

	$stats{'scanned'}++;

	#change the file content, overwrite the old file
	morph_file($fname, defined $auto ? 'auto' : 'default');

}

sub read_test_files {
	my($f) = shift;
	my($fh) = IO::File->new();
	$fh->open($f) or die("can't open file ${f} : $!\n");
	my($fcontent) = [];
	while ( <$fh> ) {
		chomp;
		next unless m{^/\S+$};
		push @{ $fcontent }, $_;
	}
	$fh->close();
	return $fcontent;
}


sub find_safe {
	foreach my $dir (@dirs) {
		morphy_log "STATUS\tWorking on directory: $dir";
		{ local $_ = $dir; morph_namespace(); }
		find(\&morph_namespace, $dir);
		morphy_log_status("Completed directory: $dir");
	}
}

sub print_summary {
	return unless defined $print_summary;
	#if user needs summary, elevate verbosity temporarily
	my $old_verbose_level = defined $verbose ? $verbose: undef;
	$verbose = 1;

	morphy_log_summary("Total files scanned		: $stats{'total'}");
	morphy_log_summary("Total files changed		: $total_files_with_changes");
	morphy_log_summary("Files not changed			: $stats{'nochange'}");
	morphy_log_summary("Unable to change(Error): $stats{'errors'}");
	syslog(LOG_INFO, "Total files: $stats{'total'}, links $n_links, changes to $total_files_with_changes files, no changes to $stats{'nochange'} files, errors $stats{'errors'}");
	$verbose = $old_verbose_level;
}

#
main:

morphy_arg_parse(@ARGV);
morphy_log_init();
morphy_read_config();
validate();

syslog(LOG_INFO, "Started processing with config ${conf} on dirs (@{dirs})");
$stats{'errors'} = 0;
$stats{'nochange'} = 0;
$stats{'total'} = 0;

if (scalar @dirs) {
	find_safe(@dirs);
}
else {
	morphy_log_info("Reading file list from $infile");
	my $file_list = read_test_files($infile);
	morphy_log_info("Found " . scalar( @{ $file_list }) . " files");
	for my $file (@{ $file_list }) {
		#morphy_log_info("Processing $file");
		morph_namespace($file);	
	}
}

print_summary();
syslog(LOG_INFO, "Ended processing with config ${conf} on dirs (@{dirs})");

__END__
#
#USAGE text goes here
#

=head1 NAME

morphy: Change pathnames from one namespace to another, B<within contents of files>,
across many directories given.

=head1 SYNOPSIS

morphy [options]

=over 

=item B<-h|--help>

Print this help message and exit

=item B<-v|--verbose>

Print debug messages (at level LOG_DEBUG)

=item B<-c|--conf cfile>

Use I<cfile> as configuration file. The config file is "\t" delimited
entries of the form:

	/from/path/oldspec	/to/path/newspec
	/from/somewhere	/to/someplace

We only support absolute paths for now.

=item B<-d|--dir DIR1...>

Perform the operations on the directories given with '-d'. You can
provide multiple directories in one run.

=item B<-i|--infile infile>

Perform the operations on the files listed in <infile>. Each line of
<infile> is expected to point to a valid file in the system.

=item B<-f|--force>

Actually perform the operations on files that need to be changed.
The default behavior is to report the potential actions, but not
really perform them. 


=item B<-l|--log logfile>

Log to I<logfile>. Default is to log to STDOUT.

=item B<-s|--summary>

Print summary statistics at the end of run.

=back

=head1 Examples

=over 


=item B<A sample config file>:

	# any file content referring to pathname  /proj/design_a should 
	# instead refer to /cloud/new/ax_design_a
	#
	# any file content that mentions path /earth/foo/bar should
	# instead refer to /cloud/foobar
	#
	# any file content that mentions path /earth/foo should instead
	# refer to /onprem/ax_foo
	# 
	# Note the last two maps: you can have a 'subdirectory' mapping
	# for a top-level directory, and that will be done BEFORE the
	# top level mapping. 

	# In this example:
	#
	# references to /earth/foo/b.c will be changed to /onprem/ax_foo/b.c
	# whereas /earth/foo/bar/x.py will be changed /cloud/foobar/x.py
	#
	#	This is the config file:
	#
	/proj/design_a	/cloud/new/ax_design_a
	/earth/foo	/onprem/ax_foo
	/earth/foo/bar	/cloud/foobar

=item B<1. morphy -c /tmp/conf.txt -d /common/project_f -s -v -l /tmp/morphy0.log>

=over

Using /tmp/conf.txt as the above path mapping, recurse through
/my/build_area and /common/project_f for all TEXT files under <SIZE> bytes,
and report the files that require changes. Print summary statistics at end,
and log everything to STDOUT.


Note that without the '-f', this won't I<actually make any changes>. This is
the default behavior.

=back

=item B<1. morphy -c /tmp/conf.txt -d /common/project_f -s -v -f -l /tmp/morphy0.log>

=over

Same as above, but this time, instead of reporting the files that need the change,
actually I<go ahead and change them in-place>. The file's timestamps will remain
the same (atime, mtime) after the change.

WARNING: This is an irreversible operation. Do it only after verifying and testing
on a sample directory.

=back


=back




=head1 DESCRIPTION

This program will change uid and/or gid of every directory it is given.

=head1 AUTHOR

Ramki Balasubramanian

=cut

#END USAGE SECTION
