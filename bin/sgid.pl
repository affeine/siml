#!/usr/bin/perl -w
    eval 'exec perl -S $0 "$@"'
        if 0;
#
my $filename = @ARGV ? shift @ARGV : '/tmp/null';

die "File ${filename} error: $!\n" unless ( -f $filename);
my $perms    = ((stat($filename))[2] & 07777); 
my $newperms = $perms ^ 02000;                 # Add the group sticky 
chmod $newperms, $filename or die "Could not change permissions: $!";
