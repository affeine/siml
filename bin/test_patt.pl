#!/usr/bin/perl -w
use strict;

my(@lines) = (
	'This works:/foo/bar/test/a.c',
	'This will also work: /foo/bar/tricks/x.c'
	);

my %map = ( 
	'/foo/bar/test' => '/cloud/bartest',
	'/foo/bar' => '/ground/bar'
);

for (@lines) {
	while (m{/[\/\w]+}gc) {
		print <<EOF;
	$_\tPrematch\t$`
	$_\tMatched \t$&
	$_\tPostMatch\t$'

EOF
	}
}

