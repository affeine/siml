#!/usr/bin/perl -w
    eval 'exec perl -S $0 "$@"'
        if 0;
#
# xprint: print selected columns (or their histogram)
#         of a field-delimited file.
#
# equivalent to a simple 'select count(*),col' or 'select col,...'
#
#
# $Id$
#
use strict;
use Getopt::Long;
use vars qw(*FH);
use Time::HiRes qw(gettimeofday usleep);

my(@header, @data, $no_header, $print_ratios);
my($help, @column_spec, @colname_spec, $IFS);
my($input_file);
my($tstamp, $xray, $debug, $query);
my(%stats);

my $verbose = 0;
my $n_columns = 0;
my $NULLDEFAULT = '-NaN-';

local(*FH);

sub _log {
	return unless ($debug);
	my $t = join(".", gettimeofday());
	print map { "$t\t$_\n" } @_;
}

sub Usage {
print("** $_[0]\n\n") if @_;
	print <<_Usage;
Usage: 
	$0 [-F sep] [-v|-q] -c col1 col2 .. col_i [-z] [-p] [-i file]
	$0 [-F sep] [-v|-q] -n name1 name2 .. name_i [-z] [-p] [-i file]
		-h: this help message
		-i: filename (default is to process STDIN)
		-v: print all rows
		-z: no header, all lines are data
		-c: list of columns (indexed from 0,1...etc)
		-n: use column NAMES instead of index(must match header line exactly)
		-p: print ratio/percentage (not useful with -v)
		-F: separator char
		-X: print xray of header line
		-d: enable debug logging
		-q: print column numbers containing string
_Usage
	exit;
}

sub Configure {
	_log("Args remaining: @ARGV\n");
	if (@ARGV) {
		_log("INPUT FILE not given, using '@ARGV'\n");
		$input_file = shift @ARGV;
	}
	if ($input_file) {
		Usage(" FILE [$input_file]: $!") unless (-r $input_file);
		open(FH, $input_file) or die("**ERROR reading $input_file : $!\n");
	} else {
		*FH = *STDIN;
	}
	#default Unix format
	$IFS = "\t" unless $IFS;
	$no_header = 0 unless $no_header;
	_log("Configuring, IFS='${IFS}', No Header = $no_header");
}

sub validate_column_spec {
	my(@good, @bad);
}


Usage() if (!GetOptions(
		'v' => \$verbose,
		'h' => \$help,
		'z' => \$no_header,
		'c=s{1,}' => \@column_spec,	#multiple/variable number of -c values
		'n=s' => \@colname_spec,
		'F=s' => \$IFS,
		'i=s' => \$input_file,
		'd' => \$debug,
		'X' => \$xray,
		'q=s' => \$query,
		)) or $help;

Usage(" You cannot specify columns by number, and the name!") if (@column_spec and @colname_spec);

Configure();

while ( <FH> ) {
	chomp;
	@data = split /$IFS/;
	if ($. == 1) {
		$n_columns = scalar @data;
		unless ($no_header) {
			@header = @data;
			if ($xray) {
				print(join("\n", map { "Column[$_] = $header[$_]" } 0..$#header), "\n");
				exit(0);
			}
			else {
				_log(join("\t", @header));
			}
			next;
		}
		else {
			@header = map { "column_" . $_ } 0..$#data;

			goto PROCESSING;	#syntactic sugar
		}
	}
	PROCESSING: {
		if ($verbose) {
			if (@column_spec) {
				print(join($IFS, @data[@column_spec]),"\n");
			}
		}
		else {
			for my $col (@column_spec) {
				$stats{$col}{$data[$col] ||= $NULLDEFAULT }++;
			}
		}
	}
}

exit(0) if $verbose;

#STATS
for my $column (@column_spec) {
	print("#Stats for Column: $header[$column]\n");
	print(join($IFS, "Value", "Frequency"), "\n");
	for my $key (sort { $stats{$column}{$a} <=> $stats{$column}{$b} } keys( %{ $stats{$column}} )) {
		print(join($IFS, $stats{$column}{$key}, $key),"\n");
	}
}

