#!/usr/bin/perl -w

use IO::File;

#use Sys::Syslog;
#use POSIX;

use Getopt::Long;
use strict;
use Time::HiRes qw(time);


my(%path_map, $content, $path_map, $path_map_fh, $help, $change);
my($default_patt, $auto_patt, $pre, $match, $post, $infile, $inputs);
my($auto_maxlen, $auto_minlen, $algo, $verbose, $force);
my($perr_no, $perr);
my($input_morphed_copies) = [];

sub Usage;
sub dprint;
sub read_map;
sub morph_file;
sub read_test_files;
sub inject_paths_into;

sub dprint {
	return unless defined $verbose;
	my $t = sprintf("%.6f", time());
	print(STDERR $t, "\t", @_);
}

sub Usage {
	my $prog = (split m{/}, $0)[-1];
	my $message = @_ ? join("\n", @_) : "";
	die <<EOU;
$message

Usage: $prog [-h] [-m map] [-f list_of_files] [-c] file

reports/changes "from" paths in <map> file to "to" paths.

	-h: this help message
	-m: map file (See example)
	-i: list of files to inject paths into
	-c: change, not report
	-a: algorithm [can be 'auto' or 'default']
	-v: verbose (print debug messages)
	file: file to change/report

example map file format:
	#comment is ok
	/from/path	/to/path2
	/down/here	/up/there
	/x/y/foo	/a/b/goo	#special case for subdir 'foo'
	/x/y/bar	/new/snark	#special case for subdir 'bar'
	/x/y	/c/d	#catch all for /x/y

EOU
}

sub read_map {
	my $f = shift;
	my $fh = IO::File->new();
	unless ($fh->open($f)) {
		$perr_no = 0;
		$perr = $!;
		goto END;
	};
	#global
	while ( <$fh> ) {
		chomp;
		dprint("Line $. ignored: $_\n"), next unless m{^/\S+\t/\S+(\s+\#)?};
		my($f, $t) = (split /\t/, $_)[0,1];
		$path_map{$f} = $t;
		dprint "Found mapping in line $.:\t$f\t$t\n";
	}
	$fh->close();
	$perr_no = 1;
	$perr = "Success";

	my($maxlen, $minlen, $len) = (0, 1000_000, 0);
	for my $k (keys %path_map) {
		$len = scalar split m{/}, $k;
		$maxlen = $len if $len > $maxlen;
		$minlen = $len if ($len > 0 and $len < $minlen);
	}
	#
	#actual number of '/' found in the old namespace
	#
	$maxlen--;
	$minlen--;
	$auto_patt = '(/[\w\-\.\_]+)';
	$auto_minlen = $minlen;
	$auto_maxlen = $maxlen;
		
	$default_patt = join '|', sort { length $b <=> length $a } keys %path_map ;
	$default_patt = '(' . $default_patt . ')';
	dprint("Default Pattern = ", $default_patt, "\n::END_PATT\n");
	dprint("Generic Pattern = ", $auto_patt, " [between $auto_minlen and $auto_maxlen deep]\n::END_PATT\n");
END: 
	return($perr_no);
}

sub read_test_files {
	my($f) = shift;
	my($fh) = IO::File->new();
	$fh->open($f) or die("can't open file ${f} : $!\n");
	my($fcontent) = [];
	while ( <$fh> ) {
		chomp;
		next unless m{^/\S+$};
		push @{ $fcontent }, $_;
	}
	$fh->close();
	return $fcontent;
}

sub inject_paths_into {
	my($test_files) = shift;
	my(@paths) = keys %path_map;
	my($nentries) = scalar(@paths);
	srand($$^time);
	my($n_rand) = 3;
	my(@lines);
	my(@rand_offsets, @rand_paths);

	my $fh = IO::File->new();
	dprint("INJECT BEGIN>> Injecting random path data into ", scalar(@{ $test_files }), " files\n");

	#GLOBAL var @test_files XXXXX
	for my $file (@{ $test_files} ) {
		@rand_offsets = ();
		@rand_paths = ();

		$fh->open($file) or next;

		@lines = <$fh>;

		my $nlines = @lines;

		$fh->close();

		for (1..$n_rand) {
			push @rand_offsets, int(rand($nlines));
			push @rand_paths, map { $paths[$_] } int(rand($nentries));
		}
		@rand_offsets = sort { $a <=> $b } @rand_offsets;
		dprint("INJECT: ${file}:${nlines}> writing at offsets = @rand_offsets. \nINJECT: paths = @rand_paths\n");

		unless ( $fh->open($file)) {
			warn("Can't open file $file: $!, skipping\n");
			next;
		};
		my(@content) = <$fh>;
		$fh->close();

		my $j2 = 0;
		for my $i (0..$#rand_offsets) {

			my $offset = $rand_offsets[$i-1];
			my $oldpath = $rand_paths[$i-1];
			my $newpath = $path_map{ $oldpath };	#must exist

			splice(@content, $offset + $j2, 0, (
				"/* these strings must not be identical: ${oldpath}.random != ${newpath}.random */\n",
				"/* these two lines must be identical:   $oldpath/foo.test */\n",
				"/* these two lines must be identical:   $newpath/foo.test */\n",
				));

			$j2 += 3;	#above lines added at offset. Re-calibrate the next offset
		}
		my($atime, $mtime) = (stat $file)[8, 9];
		my($newfile) = "${file}.morphy";

		unless ($fh->open(">$newfile")) {
			warn("Can't open output file $newfile: $!\n");
			next;
		}
		$fh->print(@content);
		$fh->close();

		#keep timestamp same as original file
		utime($atime, $mtime, $newfile);

		@content = undef;
		dprint("#check: diff $newfile ${file}\n");
		push @{$input_morphed_copies}, $newfile;
	}
	dprint(sprintf "INJECT END: %d more files\n", scalar(@{$input_morphed_copies}));
}

sub morph_file {
	my($f, $auto) = @_;
	my($fh, $content);

	unless (open($fh, $f)) {
		warn("File ${f} open failure: $!\n");
		return;
	};

	local $/ = undef;
	$content = <$fh>;
	$fh->close();
	if (defined $auto and $auto eq 'auto') {
		for (my $i = $auto_maxlen; $i >= $auto_minlen; $i--) {
			my $var_patt = "($auto_patt" . "{$i})";
			dprint("DYNAMIC>\tPattern: $var_patt\n");
			#$content =~ s,${var_patt}\b,exists $path_map{$1} ? $path_map{$1} : "#DEBUG $1 #DEBUG",egsmx;
			$content =~ s,${var_patt}\b,exists $path_map{$1} ? $path_map{$1} : $1,egsmx;
		}
	}
	else {
		#brute force enumeration of all paths
		$content =~ s,${default_patt}\b,$path_map{$1},eogsmx;
	}
	dprint("MORPHY> ${f} will be changed!\n");
	unless (open($fh, "> $f")) {
		dprint("FAILED> ${f} write error: $!\n");
		return;
	}
	$fh->print($content);
	$fh->close();
	dprint("CHANGED> ${f} changed\n");
}

Usage() if (!GetOptions(
	'h' => \$help, 
	'c' => \$change, 
	'm|map=s' => \$path_map,
	'i|infile=s' => \$infile,
	'a|algo=s' => \$algo,
	'v' => \$verbose,
	'f' => \$force,
	)
	or defined $help);

read_map($path_map);

if (defined $infile) {
	dprint("Test file: $infile\n");

	$inputs = read_test_files($infile);
	#
	#uses @test_files
	#
	inject_paths_into($inputs);
	dprint(sprintf "PROCESS BEGIN: %d original files\n", scalar(@{$inputs}));
	push(@{$inputs}, @{$input_morphed_copies}) or die("push failed: $!\n");
	dprint("UPDATE_COUNT_FILES> Added morphed files to original list of files\n");
};

$algo = 'default' unless defined $algo and $algo eq 'auto';

if (defined $force) {
	dprint(sprintf "PROCESS BEGIN: %d morphed  files\n", scalar(@{$input_morphed_copies}));
	for my $file (@{$input_morphed_copies}) {
		morph_file($file, $algo ||= 'default');
		dprint("CHANGED\t$file\t$algo\n");
	}
}
else {
	dprint("WILLCHANGE\t$_\t$algo\n") for @{$input_morphed_copies};
}

