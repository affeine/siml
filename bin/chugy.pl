#!/usr/bin/perl -w
		eval 'exec perl -S $0 "$@"'
				if 0;
#
# chugy.pl: change uid and gid
#
# $Id$
#

use Pod::Usage qw(pod2usage);
use Getopt::Long;
use Sys::Syslog qw(:standard :macros);
use Sys::Hostname;
use File::Find;
use Time::HiRes qw(time);
use POSIX;

use strict;


#
#fn declarations here
#
sub siml_log;
sub validate;
sub siml_arg_parse;
sub siml_log_init;
sub siml_read_config;
sub Usage;

sub find_safe;
sub find_fast;	#to be implemented 
sub change_uid_gid;
#
#

#
#vars here
#
my(%uidmap, %gidmap, %uidgid, %user, %nuser, %group, %stats, @exclude_paths, $exclude_patt);
my($conf, $log, $log_h, @dirs, $help, $program, $infile, $undo);
my($force, $verbose);
my($follow_symlinks) = 0;
my($print_summary, $n_processed, $n_links) = (0,0,0);
my $GRAIN = 5000;

my($uidlast, $gidlast, $uid, $gid);

#
# sub definitions here
#

sub Usage {
	if ($_[0] and $_[0] =~ /^\d+/) {
		pod2usage('-verbose' => shift);
	}
	else {
		pod2usage('-verbose' => 1, '-message' => $_[0]);
	}
}

sub Quiet {
	die(join "\n", @_, "\nTry ${0} -h for more information\n");
}

sub siml_arg_parse {
	local(@ARGV) = @_;
	Usage() if (!GetOptions(
		'c|conf=s' => \$conf,
		'd|dir=s' => \@dirs,
		'e|exclude=s' => \@exclude_paths,
		'f|force' => \$force,
		'g|grain=i' => \$GRAIN,
		'h|help' => \$help,
		'i|infile=s' => \$infile,
		'l|log=s' => \$log,
		's|summary' => \$print_summary,
		'v|verbose' => \$verbose,
		'u|undo' => \$undo,
		));
	Usage(2) if defined $help;
	Usage("Config File error: $!") unless (defined $conf and -r $conf);

	unless (defined $infile) {
		Usage("No directories to operate on!") unless (@dirs and grep -d, @dirs);
	};
	if (defined $undo and !defined $infile) {
		Usage("ERROR. Need a logfile from previous run to undo the changes!");
	}

	Usage("Log {$log} exists, won't overwrite!") 
		if (!defined $force and defined $log and -f $log);

	siml_log_info("Dry run mode, no changes will be made!") unless defined $force;
}

#
#config file format is described in Pod doc below.
#There are two ways to specify a change map:
#
#	existing {user or gid} maps to new {user or gid}
#
#	exising user:gid combination maps to new user:gid
#

sub siml_read_config {
	my $conf_h;
	my($type, $old, $new, $ouid, $ogid, $nuid, $ngid, @map);

	open($conf_h, '<', $conf) or die("Cannot read $conf: $!\n");
	siml_log_info(">> Begin ReadConfig : ${conf}\n");
	while ( <$conf_h> ) {
		chomp;
		if ( /^uidgid:(\d+):(\d+):(\d+):(\d+)/ ) {
			if (defined $undo) {
				#flip the old/new for an UNDO operation
				($nuid, $ngid, $ouid, $ogid) = ($1, $2, $3, $4);
			} else {
				($ouid, $ogid, $nuid, $ngid) = ($1, $2, $3, $4);
			}
			Quiet("\n** WARNING: duplicate mapping exists for [$ouid] = @{$uidgid{$ouid}}")
				if exists $uidgid{$ouid};
			$uidgid{$ouid} = [ $nuid, $ogid, $ngid];
			#siml_log_info("Map $ouid: @{$uidgid{$ouid}}");
		}
		elsif ( /^([ug]id):(\d+):(\d+)/ ) {
			if (defined $undo) {
				#flip the old/new for an UNDO operation
				($type, $new, $old) = ($1, $2, $3);
			} else {
				($type, $old, $new) = ($1, $2, $3);
			}
			#siml_log_info("MapEntry> $_\n";
			if ($type eq 'uid') {
				Quiet("\n** WARNING: duplicate User mapping exists for '$user{$old}' (uid $old)\n")
					if exists $uidmap{$old};
				$user{$old} = getpwuid($old);
				$uidmap{$old} = $new;
			}
			elsif ($type eq 'gid') {
				Quiet("\n** WARNING: duplicate Group mapping exists for '$group{$old}' (gid $old)\n")
					if exists $gidmap{$old};
				$group{$old} = getgrgid($old);
				$gidmap{$old} = $new;
			}
		} elsif ( /^exclude_path:\s*(\S+)$/) {
			#exclude paths containing specific strings
			push @exclude_paths, $1;
		}
	}
	if (@exclude_paths) {
		$exclude_patt = '(' . join('|', map { s/([\.\W])/\\$1/g; $_} @exclude_paths) . ')/';
		print STDERR "CONF\tExcluded paths: $exclude_patt\n";
	}
	siml_log_info("<< End ReadConfig: ${conf}\n");
	if (exists $ENV{'X_DEBUG_EXTENSIVE'}) {	#turn on debug config file reads
		print STDERR "CONF\tIgnored paths: @{exclude_paths}\n" if @exclude_paths;
		for my $u (keys %uidmap) { print STDERR "CONF\tUID old($u) -> new($uidmap{$u})\n"; }
		for my $g (keys %gidmap) { print STDERR "CONF\tGID old($g) -> new($gidmap{$g})\n"; }
		for my $ug (keys %uidgid) { print STDERR "CONF\tUIDGID old($ug) -> new(@{$uidgid{$ug}})\n"; }
	}
}

sub siml_log_init {
	($program = $0 ) =~ s,^.+/,,;
	if (defined $log) {
		open($log_h, '>', $log) or die("Cannot write to log {$log}: $!\n");
#		siml_log ">>> Opening logfile ${log} for writing\n";
	}
	else {
		$log_h = *STDOUT;
		print STDERR ">>> Opening STDOUT for writing\n";
	}

	#open syslog
	openlog($program, 'ndelay,pid', 'user');

}

sub siml_log {
	#simple logger
	return unless defined $verbose;

	my $t = sprintf("%.6f", time());
	defined $log_h ?
		 print $log_h join("\t", $t, @_, "\n") :
		 print STDERR join("\t", $t, @_, "\n");
}

#AUTOLOAD some day
sub siml_log_info { siml_log "INFO", @_; }
sub siml_log_changed { siml_log "CHANGED", @_; }
sub siml_log_nochange { siml_log "NOCHG", @_; }
sub siml_log_debug { siml_log "DEBUG", @_; }
sub siml_log_status { siml_log "STATUS", @_; }
sub siml_log_summary { siml_log "SUMMARY", @_; }
sub siml_log_scan { siml_log "SCANNED", @_; }

sub validate {
	if (($< == 0 or $> == 0) and defined $force) {
		siml_log_info("Running as root. No undo possible!");
	}
	if (@dirs) {
		die("Cannot modify any directories given!\n") unless grep -w, @dirs;
	}
}

#
#This is the core. Make it count.
#
sub change_uid_gid {

	my($fname, @rest);
	if (@_) {
		($fname, @rest) = @_;
		#print STDERR "Called with ARGS: @_\n";
	};

	$n_processed++;
	if (-l) {
		#do nothing for symlinks, yet
		$stats{'nochange'}++;
		return;
	}

	#exclude certain directories by default
	$n_processed--, return if ( m{^\.\.?$} );
	$n_processed--, return if ( m{^\.snapshot$} );
	$n_processed--, return if ( m{^\.fastremove$} );
	$n_processed--, return if ( defined $exclude_patt and m{$exclude_patt});

	syslog(LOG_INFO, "Processed ${n_processed} inodes") 
		if ($n_processed % $GRAIN == 0);

	my($ngid, $nuid);

	#this function is called differently when asked to crawl
	#the filesystem, versus when it is simply doing an undo
	#or replay/re-apply operation on previous run's log
	#
	#since previous run's log includes the full file path,
	#we don't need File::Find for capturing the fullpath
	#or as a replay/undo 

	if (defined $fname) {
		$_ = $fname;
	} else {
		$fname = $File::Find::name;
	}

	($uid, $gid) = (stat $_)[4,5];
	
#	siml_log "FILE>\t$fname\t$uid\t$gid";

	#
	#if unique old u:g -> new u:g map exists, use that
	#
	#the map is %{oldid} -> [newuid, oldgid, newgid]
	#
	#siml_log_info("checking uid map for [${uid}:${gid}]");
	#siml_log_info("--> found @{ $uidgid{$uid} }") if exists $uidgid{$uid};

	if (exists $uidgid{$uid} and $uidgid{$uid}->[1] == $gid) {
		$nuid = $uidgid{$uid}->[0];
		$ngid = $uidgid{$uid}->[2];
		#siml_log_info("Matched uid+gid [${uid}:${gid}] -> [${nuid}:${ngid}]")
	}
	else {
		$nuid = defined $uidmap{$uid} ? $uidmap{$uid} : $uid;
		$ngid = defined $gidmap{$gid} ? $gidmap{$gid} : $gid;
		#siml_log_info("No uid+gid map [${uid}:${gid}] -> [${nuid}:${ngid}]")
	}

	if ($nuid != $uid or $ngid != $gid) {
		if (defined $force) {
			#------------------------------------
			#CORE: CHANGE FILE UID or GID or both
			#------------------------------------
			if ( chown($nuid, $ngid, $fname)) {
				siml_log_changed(join("\t", $fname, $uid, $gid, $nuid, $ngid));
				$stats{'uid'}{$uid}++ if $nuid != $uid;
				$stats{'gid'}{$gid}++ if $ngid != $gid;
				$stats{'uidgid'}{"$uid:$gid"}++ if $nuid != $uid;
				$stats{'changed'}++;
			}
			else {
				siml_log_error(join("\t", $fname, $!));
				$stats{'error'}{"$!"}++;
			}
		}
		else {
			#record, don't report
			#siml_log_nochange(join("\t", $fname, $uid, $gid));
			$stats{'nochange'}++;
		}
	}
	elsif ($nuid == $uid and $ngid == $gid) {
		#log the stats for unmapped uid/gid
		$stats{'unmapped_uid'}{$uid}++;
		siml_log_nochange join("\t", $fname, $uid, $gid) if defined $force;
		$stats{'nochange'}++;
	} else {
		 siml_log_nochange join("\t", $fname, $uid, $gid) if defined $force;
		 $stats{'nochange'}++;
	}
}

sub find_safe {
	foreach my $dir (@dirs) {
		siml_log "STATUS\tWorking on directory: $dir";
		{ local $_ = $dir; change_uid_gid($_); }
		find(\&change_uid_gid, $dir);
		siml_log_status("Completed directory: $dir");
	}
}

sub print_summary {
	return unless defined $print_summary;
	#if user needs summary, elevate verbosity temporarily
	my $old_verbose_level = defined $verbose ? $verbose: undef;
	$verbose = 1;

	my($total, $nochange, $changed, $errors) = (0, 0, 0, 0);
	$errors += $stats{'error'}{$_} for keys %{ $stats{'error'}};
	$nochange = $stats{'nochange'} || 0;
	$changed = $stats{'changed'} || 0;
	$total = $errors + $nochange + $changed;
	siml_log_summary("Total files scanned		: $total");
	siml_log_summary("Total files changed		: $changed");
	siml_log_summary("Files not changed			: $nochange");
	siml_log_summary("Unable to change(Error): $errors");
	siml_log_summary("#\n#---- Files with UID:GID changes ---");
	for my $k (sort { $stats{'uidgid'}{$a} <=> $stats{'uidgid'}{$b} } keys %{ $stats{'uidgid'}}) {
		siml_log_summary( sprintf("Files changed for uid:gid %-11s : %s", $k, $stats{'uidgid'}{$k} || 0));
	}
	siml_log_summary("#\n#---- Files with userid changes ---");
	for my $k (sort { $stats{'uid'}{$a} <=> $stats{'uid'}{$b} } keys %{ $stats{'uid'}}) {
		siml_log_summary( sprintf("Files changed for uid %-16d : %s", $k, $stats{'uid'}{$k} || 0));
	}
	siml_log_summary("#\n#---- Files with group id changes ---");
	for my $k (sort { $stats{'gid'}{$a} <=> $stats{'gid'}{$b} } keys %{ $stats{'gid'}}) {
		siml_log_summary( sprintf("Files changed for gid %-16d : %s", $k, $stats{'gid'}{$k} || 0));
	}
	siml_log_summary("#\n#---- Files with UID that had no mapping from old->new ---");
	for my $k (sort { $stats{'unmapped_uid'}{$a} <=> $stats{'unmapped_uid'}{$b} } keys %{ $stats{'unmapped_uid'}}) {
		siml_log_summary( sprintf("Files with unmapped_uid %-14d : %s", $k, $stats{'unmapped_uid'}{$k} || 0));
	}
	syslog(LOG_INFO, "Total files: $n_processed, links $n_links, changed $changed, unchanged $nochange, errors $errors");
	$verbose = $old_verbose_level;
}

sub stream_process {
	my $f = shift;
	my $fh;

	#patterns must match the strings used in siml_log_{change|nochange} routines
	#TODO: cleanup when implementing AUTOLOADed subs

	my $rx = defined $undo ? 'CHANGED' : 'NOCHG';
	open($fh, '<', $infile) or die("Cannot read log '${infile}': $!\n");
	while ( <$fh> ) {
		chomp;
		next unless /\t${rx}\t/o;
		my(@args) = (split /\t/, $_);

		#discard timestamp and facility from log entry
		splice(@args, 0, 2);
#		print STDERR "Will change uid gid: @args\n";
		change_uid_gid(@args);
	}
}
	

#
main:

siml_arg_parse(@ARGV);
siml_log_init();
siml_read_config();
validate();

if ($infile and -r $infile) {
	print STDERR "Reading {$infile}\n";
	
	syslog(LOG_INFO, "Started undo/redo processing with previous run's output: ${infile}");
	stream_process($infile);
	syslog(LOG_INFO, "Started undo/redo processing with previous run's output: ${infile}");
}
else {
	syslog(LOG_INFO, "Started processing with config ${conf} on dirs (@{dirs})");
	find_safe(@dirs);
	syslog(LOG_INFO, "Ended processing with config ${conf} on dirs (@{dirs})");
}

print_summary();

__END__
#
#USAGE text goes here
#

=head1 NAME

chugy: Change UID and GID across many directories and many user/group ids

=head1 SYNOPSIS

chugy [options]

=over 

=item B<-h|--help>

Print this help message and exit

=item B<-v|--verbose>

Print debug messages (at level LOG_DEBUG)

=item B<-c|--conf cfile>

Use I<cfile> as configuration file. The config file is ':' delimited
tuples, and should have lines of the following formats:
	1. uid:NNNold:NNNnew
	2. gid:NNNold:NNNnew
	3. uidgid:NNNolduid:NNNoldgid:NNNnewuid:NNNnewgid
	4. exclude_path:filename

Note that the first part of each line should be the literal 'uid',
'gid', or 'uidgid'. Any other keyword in the first field is excluded.

The "exclude_path" lines contain path names to be IGNORED for scanning.
Common examples are .snapshot, .fastremove. You can also specify this
using the -e|--exclude


=item B<-d|--dir DIR1...>

Perform the operations on the directories given with '-d'. You can
provide multiple directories in one run.

=item B<-e|--exclude PATH1...>

Exclude filenames that contain the provided path prefix (top level). These
should be valid path names I<without whitespace>. For example, 
a "-e .snapshot" will exclude all files under .snapshot directory.

=item B<-f|--force>

Perform the UID/GID change operations on files. Without the I<-f>,
potential changes will be reported, but no changes will be made.

=item B<-i|--infile LOGFILE> 

Read the logfile from a previous run, filter the files that
have NOT been changed (as reported in the log), and apply those
changes to a new uid/gid map that now includes the missed uid/gid
from the previous run(s). This is necessary if a previous run
missed out uid/gid and you I<do not want to re-scan the entire
file system/directory>. 

=item B<-l|--log logfile>

Log to I<logfile>. Default is to log to STDOUT.

=item B<-s|--summary>

Print summary statistics at the end of run.

=item B<-u|--undo>

Flip the from/to mapping from the config file (-c option)
and run the uid/gid change in reverse. That is, the map
file is now interpreted as I<to_uid,to_gid,from_uid,from_gid>.
This would be necessary in extreme cases where you want to
completely undo the last run. If you ran the forward pass
with "-l", simply provide that file with the "-i" option.


=back

=head1 Examples

=over 


=item B<A sample config file>:

	#files owned by uid 1222 will be changed to 22222 ownership
	#files with group ownerhip of 9876 will be changed to group 12345
	uid:1222:22222
	gid:9876:12345

=item B<a more elaborate config file>:

	#example:
	#	files owned by johndoe(1000 uid) move to jdoe (12999)
	#	files with their group kia (104) move to group tesla (65534)
	#	user maryj (id 9999, primary group 104) 
	#			 moves to mjane(id 29999, primary group 10006)
	#	user balrog (id 12000, primary group 104)
	#			 moves to sith(31010), primary group 666
	#	This is the config file:
	#
	uidgid:12000:104:31010:666
	uidgid:9999:104:29999:10006
	uid:1000:12999
	gid:104:65534

=item B<1. chugy -c /tmp/conf.txt -d /cloud/c1vol -d /ground/builds -s -v>

=over

Use /tmp/conf.txt as the uid/gid mapping file, SHOW what operation 
will be performed on files under directories /cloud/c1vol and /ground builds,
print summary statistics at the end of the run.

Note that without the '-f', this won't I<actually make any changes>. This is
the default behavior.

=back

=item B<2. chugy -c /me/cloud.conf -d /cloud/x -s -f -v -l /tmp/log.txt>

=over

Use /me/cloud.conf as the uid/gid mapping file, run the uid/gid change
on /cloud/x directory and all files under it, log all output to
/tmp/log.txt. Note that warning/critical messages will continue to
be printed to STDERR.

=back


=back




=head1 DESCRIPTION

This program will change uid and/or gid of every directory it is given.

=head1 AUTHOR

Ramki (perl version). RaviTeja and Manish tested the implementation to
provide additional requirements for the undo and re-scan flow. RaviTeja
implemented the deployment scheme using Redis/message-queues.



=cut

#END USAGE SECTION
